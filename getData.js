const http = require('http');
const { promises } = require('fs');

const { v4: uuidv4 } = require('uuid');

const routeHandlers = {
    'html': async (req, res) => {
        const htmlData = await promises.readFile('./index.html', 'utf-8');
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end(htmlData);
    },

    'json': async (req, res) => {
        const jsonData = await promises.readFile('./jsonResponse.json', 'utf-8');
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(jsonData);
    },

    'uuid': (req, res) => {
        const uuidResponse = { uuid: uuidv4() };
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify(uuidResponse));
    },

    'status': (req, res) => {
        // Status code will be stored in 2 position of req.url
        const statusCode = parseInt(req.url.split('/')[2], 10);
        // If status code is wrong shows Invalid status code
        res.writeHead(statusCode, { 'Content-Type': 'text/plain' });
        res.end(`Response with status code: ${statusCode}`);
    },

    'delay': (req, res) => {
        const delaySeconds = parseInt(req.url.split('/')[2], 10);
        setTimeout(() => {
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            res.end(`Response after ${delaySeconds} seconds`);
        }, delaySeconds * 1000);
    }
};

const server = http.createServer((req, res) => {
    try {
        const urlParts = req.url.split('/');
        const route = urlParts[1];

        // Check if the route exists in the routeHandlers
        if (routeHandlers.hasOwnProperty(route)) {
            routeHandlers[route](req, res);
        } else {
            res.writeHead(404, { 'Content-Type': 'text/html' });
            res.end('<h1>404 Not Found</h1>');
        }
    } catch (error) {
        console.error(error.message);
        res.writeHead(500, { 'Content-Type': 'text/html' });
        res.end('<h1>500 Internal Server Error</h1>');
    }
});

const PORT = 3000;

server.listen(PORT, () => {
    console.log(`Server running at http://localhost:${PORT}/`);
});